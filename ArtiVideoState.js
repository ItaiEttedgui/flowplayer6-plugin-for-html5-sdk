var ArtiVideoState = {
	PLAY: "play",
	PAUSE: "pause",
	RESUME: "resume",
	STOP: "stop"
};