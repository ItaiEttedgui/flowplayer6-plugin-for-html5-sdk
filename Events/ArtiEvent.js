var ArtiEvent = {
	EVT_INIT_COMPLETE: "evtInitComplete", //attach success:Boolean as parameter, happens after initArtimedia finish loading GetNewTags or fails (ok/not ok)
	EVT_PAUSE_REQUEST: "evtPauseRequest", //request the publisher to pause the player
	EVT_RESUME_REQUEST: "evtResumeRequest", //request the publisher to resume the content
	EVT_LINEAR_AD_START: "evtLinearAdStart", //when linear creative starts playing, publisher should switch to "Ad Mode"
	EVT_LINEAR_AD_PAUSE: "evtLinearAdPause", //when linear ad paused by ArtiMedia
	EVT_LINEAR_AD_RESUME: "evtLinearAdResume", //when linear ad resumed by ArtiMedia
	EVT_LINEAR_AD_STOP: "evtLinearAdStop", //when linear ad(s) break ends, and publisher should switch back from "Ad Mode"
	EVT_AD_MISSED: "evtAdMissed", //when getAd returned no ad / timeout on calls etc
	EVT_AD_SHOW: "evtAdShow", //( adType:AdType, adInfo:*) OVERLAY / PREROLL / MIDROLL / POSTROLL / PAUSEROLL 
	EVT_AD_HIDE: "evtAdHide",
	EVT_SESSION_END: "evtSessionEnd",
	EVT_AD_CLICK: "evtAdClick",
	EVT_AD_SKIPPED: "evtAdSkipped",
	EVT_DESTROY: "evtDestroy"
};