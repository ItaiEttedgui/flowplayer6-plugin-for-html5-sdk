/*jslint browser: true, plusplus: true, bitwise: true, vars: true, debug: true, forin: true*/
/*global console, $, artimedia, artimediamulti, artimediaFP6, flowplayer, ArtiVideoState, ArtiEvent*/
//@prepros-prepend ArtiVideoState.js
//@prepros-prepend Events/ArtiEvent.js

window.artimediaFP6Instances = [];
window.amSDKLoaded = false;
var amSDKInstanceIndex = 0;

var ArtiMediaFP6 = function () {
    "use strict";
    this.amSDKInstanceName = "artiMediaSDK" + amSDKInstanceIndex;
    this.amSDKInstanceIndex = amSDKInstanceIndex;
    window.artimediaFP6Instances.push(this);
    amSDKInstanceIndex++;
};

function getAMFP6Instance(instanceName) {
    "use strict";
    var i;
    for (i = 0; i < window.artimediaFP6Instances.length; i++) {
        if (instanceName === window.artimediaFP6Instances[i].amSDKInstanceName) {
            return window.artimediaFP6Instances[i];
        }
    }
    return null;
}
ArtiMediaFP6.prototype = {
    AMFP6_TITLE: "ArtiMedia HTML5 FP6 Plugin",
    AMFP6_VERSION: "1.0.3.14", // "a.b.c.ddd": a-Generation, b-Product, c-Feature, ddd-Build
    firstPlay: true,
    isArtiLinearPaused: false,
    vidContainer: null,
    fPAPI: null,
    isIOS: false,
    latestPlayhead: -1,
    amSDKInstanceName: null,
    amSDKInstanceIndex: 0,
    amSDKInstance: null,
    FP6ControlsCopiedStyleTag: null,

    vidObj: function () {
        "use strict";
        return $(".fp-engine")[0];
    },

    init: function (params) {
        "use strict";
        
        var that = this;

        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
            this.isIOS = true;
        }

        console.log("AMFP6: ArtiMedia FlowPlayer6 HTML5 Plugin, Version: " + this.AMFP6_VERSION);

        if (typeof params.vidContainer === "string") {
            params.insertBefore = $("#" + params.vidContainer + " .fp-ui");
            this.vidContainer = document.getElementById(params.vidContainer);
        } else {
            params.insertBefore = $("#" + params.vidContainer.id + " .fp-ui");
            this.vidContainer = params.vidContainer;
        }

        function clickHandler(e) {
            that.vidContainer.removeEventListener("click", clickHandler);
            getAMFP6Instance(that.amSDKInstanceName).onFPEvent(e);
        }

        this.vidContainer.addEventListener("click", clickHandler);

        function amsdkLoaded() {
            var artiParams = params,
                eventName = "";

            that.amSDKInstance = artimediamulti(that.amSDKInstanceName);

            function onAMSDKEvent(e) {
                that.onArtiEvent.call(that, e);
            }

            for (eventName in ArtiEvent) {
                that.amSDKInstance.registerAdEvent(ArtiEvent[eventName], onAMSDKEvent);
            }

            that.amSDKInstance.init(artiParams);
        }

        if (window.amSDKLoaded === false) {
            console.log("AMFP6: Loading the amsdk.js");
            this.loadScript("//akamai.advsnx.net/CDN/" + params.siteKey + "/html5/amsdk.js", amsdkLoaded);
        } else {
            console.log("AMFP6: amsdk.js has already been loaded, proceeding..");
            amsdkLoaded();
        }
    },

    adInProgress: false,

    onArtiEvent: function (e) {
        "use strict";
        var that = this;
        switch (e.type) {
        case ArtiEvent.EVT_AD_SKIPPED:
            break;
        case ArtiEvent.EVT_SESSION_END:
            this.amSDKInstance.destroy();
            break;
        case ArtiEvent.EVT_DESTROY:
        $(this.vidContainer).css("pointer-events", "");
            //console.log("!!!!!caught 'destroy'");
            //console.log("!!!!!Trying to remove 'arti" + this.amSDKInstanceIndex + "-ui'");
            $(".arti" + this.amSDKInstanceIndex + "-ui").remove();
            if (this.FP6ControlsCopiedStyleTag) {
                document.head.removeChild(this.FP6ControlsCopiedStyleTag);
                this.FP6ControlsCopiedStyleTag = null;
            }
            clearTimeout(this.progressUpdateCallback);
            document[this] = null;
            delete document[this];
            $("#" + this.vidContainer.id + " .fp-ui").show();
            break;
        case ArtiEvent.EVT_INIT_COMPLETE:
        $(this.vidContainer).css("pointer-events", "");

            this.initCompleted = true;
            if (this.playbackQueued === true) {
                this.playbackQueued = false;
                this.fPAPI.resume();
                //console.log("!!!init completed, resetting flags and resuming!!")
            }
            break;
        case ArtiEvent.EVT_PAUSE_REQUEST:
            //if (this.fPAPI.paused === false || !this.vidObj.paused === false) {
            //$("#" + this.vidContainer.id + " .fp-ui").hide();
            if (this.fPAPI) {
                this.fPAPI.pause();
            }
            //}
            break;
        case ArtiEvent.EVT_LINEAR_AD_START:
            this.adInProgress = true;
            if (this.fPAPI) {
                this.setVolumeBar(this.fPAPI.volumeLevel);
            }
            this.updateAdProgress();
            $("#" + this.vidContainer.id + " .fp-ui").hide();
            $(".arti" + this.amSDKInstanceIndex + "-ui").show();
            break;
        case ArtiEvent.EVT_LINEAR_AD_STOP:
            this.adInProgress = false;
            $("#" + this.vidContainer.id + " .fp-ui").show();
            $(".arti" + this.amSDKInstanceIndex + "-ui").hide();
            break;
        case ArtiEvent.EVT_LINEAR_AD_PAUSE:
            this.isArtiLinearPaused = true;
            break;

        case ArtiEvent.EVT_LINEAR_AD_RESUME:
            this.isArtiLinearPaused = false;
            break;

        case ArtiEvent.EVT_RESUME_REQUEST:
            this.adInProgress = false;
            if (this.isIOS) {
                this.fPAPI.seek(this.latestPlayhead, function () {
                    that.vidObj().play(); //Don't remove or change this functionality, fPAPI.resume doesn't work with iPhone!!! last time we've wasted hours on this for nothing.
                });
            } else {
                this.fPAPI.resume();
            }
            break;
        case ArtiEvent.EVT_AD_MISSED:
            if (this.isIOS) {
                this.vidObj().play();
            } else {
                this.fPAPI.resume();
            }
            break;
        }
    },

    copyControls: function () {
        "use strict";
        var that = this;
        $.ajax({
            type: "GET",
            url: "//releases.flowplayer.org/6.0.4/skin/minimalist.css",
            dataType: "text",
            success: function (data) {
                data = data.replace(/url\(\'/g, "url('//releases.flowplayer.org/6.0.4/");
                data = data.split(".fp-").join(".arti" + that.amSDKInstanceIndex + "-");
                data = data.split(".arti" + that.amSDKInstanceIndex + "-fullscreen").join(".fp-fullscreen");
                data = data.split(".arti" + that.amSDKInstanceIndex + "-embed").join(".fp-embed");
                data = data.split(".arti" + that.amSDKInstanceIndex + "-volume").join(".fp-volume");
                data = data.split(".arti" + that.amSDKInstanceIndex + "-mute").join(".fp-mute");

                data = data.split(".fp-volumeslider").join(".arti" + that.amSDKInstanceIndex + "-volumeslider");

                var style_tag = document.createElement('style');
                style_tag.appendChild(document.createTextNode(data));
                that.FP6ControlsCopiedStyleTag = style_tag;
                document.head.appendChild(style_tag);


                var controlsOuterHTML = $("#" + that.vidContainer.id + " .fp-ui")[0].outerHTML.split("fp-").join("arti" + that.amSDKInstanceIndex + "-");
                controlsOuterHTML = controlsOuterHTML.split("arti" + that.amSDKInstanceIndex + "-fullscreen").join("fp-fullscreen");
                controlsOuterHTML = controlsOuterHTML.split("arti" + that.amSDKInstanceIndex + "-embed").join("fp-embed");
                controlsOuterHTML = controlsOuterHTML.split("arti" + that.amSDKInstanceIndex + "-volume").join("fp-volume");
                controlsOuterHTML = controlsOuterHTML.split("arti" + that.amSDKInstanceIndex + "-mute").join("fp-mute");

                controlsOuterHTML = controlsOuterHTML.split("fp-volumeslider").join("arti" + that.amSDKInstanceIndex + "-volumeslider");

                var newControls = $(controlsOuterHTML).insertAfter("#" + that.vidContainer.id + " .fp-ui").attr("id", "arti" + that.amSDKInstanceIndex + "FPControls");



                newControls.className = "arti" + that.amSDKInstanceIndex + "-ui";
                newControls.css("pointer-events", "none");
                newControls.find("*").each(function (ind, obj) {
                    $(obj).css("pointer-events", "all");
                });
                newControls.hide();
                newControls.css("z-index", $(".fp-ui").css("z-index"));
                $(".arti" + that.amSDKInstanceIndex + "-volumeslider").click(function (e) {
                    var volumePercentage = (e.offsetX / $(e.currentTarget).width());
                    that.setVolumeBar(volumePercentage);
                });
                $(".arti" + that.amSDKInstanceIndex + "-play").show();
                $(".arti" + that.amSDKInstanceIndex + "-play").click(function (e) {
                    that.playpauseAd();
                });

                $(".arti" + that.amSDKInstanceIndex + "-title").css("display", "none");
                $(".arti" + that.amSDKInstanceIndex + "-volumeslider .fp-volumelevel").css("float", "left");
                $(".flowplayer .arti" + that.amSDKInstanceIndex + "-elapsed").css({
                    "left": "22px",
                    "right": "auto"
                });
                $(".is-rtl.flowplayer .arti" + that.amSDKInstanceIndex + "-elapsed").css({
                    "left": "auto",
                    "right": "22px"
                });


            },
            error: function (error) {}
        });
    },

    setVolumeBar: function (volumePercentage) {
        "use strict";
        $(".arti" + this.amSDKInstanceIndex + "-volumeslider .fp-volumelevel").width((volumePercentage * 100) + "%");
        this.setAdVolume(volumePercentage);
        this.fPAPI.volume(volumePercentage);
    },


    updateAdProgress: function () {
        "use strict";
        var that = this;
        setTimeout(function () {
            that.progressUpdateCallback.call(that);
        }, 250);
    },

    toTimeString: function (totalTime) {
        "use strict";
        if (totalTime < 0) {
            return "00:00";
        }

        totalTime = parseInt(totalTime, 10);

        var seconds = totalTime % 60,
            minutes = (totalTime - seconds) / 60,
            output;

        if (minutes >= 10) {
            output = minutes.toString();
        } else {
            output = "0" + minutes;
        }
        output += ":";
        if (seconds >= 10) {
            output += seconds;
        } else {
            output += "0" + seconds;
        }

        return output;
    },

    progressUpdateCallback: function () {
        "use strict";
        var that = this;
        if (this.amSDKInstance && this.amSDKInstance.adContainer) {
            var currentTime = this.amSDKInstance.getAdCurrentTime(),
                duration = this.amSDKInstance.getAdDuration();
            if (isNaN(duration)) {
                duration = 1;
            }

            $(".arti" + this.amSDKInstanceIndex + "-elapsed").html(this.toTimeString(currentTime));
            $(".arti" + this.amSDKInstanceIndex + "-duration").html(this.toTimeString(duration));

            $(".arti" + this.amSDKInstanceIndex + "-progress").css("width", ((currentTime / duration) * 100) + "%");

            that.changeAdState();
            if (currentTime < duration) {
                setTimeout(function () {
                    that.progressUpdateCallback.call(that);
                }, 50);
            }
        }
    },

    loadScript: function (url, callback) {
        "use strict";
        var script = document.createElement("script");
        script.type = "text/javascript";
        var that = this;
        /*var loadingTimeout = setTimeout(function(){
            console.log("AMFP6: amsdk.js loading timeout/failed, cancelling...");
            that.fPAPI.off("pause resume progress finish volume");
            //vidContainer.addEventListener("click", clickHandler);
            that.targetFP6PlayerContainer.css("pointer-events", "");
        }, 5000);*/
        
        if (script.readyState) { //IE
            script.onreadystatechange = function () {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                    script.onreadystatechange = null;
                    window.amSDKLoaded = true;
                    //clearTimeout(loadingTimeout);
                    callback();
                }
            };
        } else { //Others
            script.onload = function () {
                window.amSDKLoaded = true;
                //clearTimeout(loadingTimeout);
                callback();
            };
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    },
    playpauseAd: function () {
        "use strict";
        if (this.amSDKInstance !== undefined) {
            if (this.isArtiLinearPaused) {
                this.isArtiLinearPaused = false;
                this.amSDKInstance.resumeAd();
            } else {
                this.isArtiLinearPaused = true;
                this.amSDKInstance.pauseAd();
            }
        }

    },

    changeAdState: function () {
        "use strict";
        if (this.isArtiLinearPaused) {
            document.styleSheets[0].addRule('.arti' + this.amSDKInstanceIndex + '-play:before', 'content: "\\e608"!important;');
        } else {
            document.styleSheets[0].addRule('.arti' + this.amSDKInstanceIndex + '-play:before', 'content: "\\e607"!important;');
        }
    },

    setAdVolume: function (volume) {
        "use strict";
        if (this.amSDKInstance && this.amSDKInstance.adContainer) {
            this.amSDKInstance.setAdVolume(volume);
        }
    },

    startCalled: false,
    initCompleted: false,
    playbackQueued: false,

    onFPEvent: function (e, api, data) {
        "use strict";

        //console.log("!!!!!!!FPEVENT: " + e.type);
        if (this.amSDKInstance && this.amSDKInstance.adContainer) {
            switch (e.type) {
            case "load":
                break;
            case "click":
                if (this.startCalled === false) {
                    this.startCalled = true;
                    //console.log("start() called from 'click' event");
                    this.amSDKInstance.start();
                    this.onFPEvent({
                        type: "resume",
                        api: api,
                        data: data
                    });
                    //this.fPAPI.trigger("resume");
                }
                break;
            case "volume":
                this.setAdVolume(data);
                break;
            case "resume":
                if (this.initCompleted === false) {
                    this.fPAPI.pause();
                    this.playbackQueued = true;
                    return;
                }
                if (this.firstPlay) {
                    this.firstPlay = false;
                    //console.log("updateVideoState(PLAY) called from 'resume' event");
                    this.amSDKInstance.updateVideoState(ArtiVideoState.PLAY);
                    return;
                }
                this.amSDKInstance.updateVideoState(ArtiVideoState.RESUME);
                break;
            case "progress":
                if (this.initCompleted === false) {
                    this.fPAPI.pause();
                    this.playbackQueued = true;
                    return;
                }
                if (this.startCalled === false && this.fPAPI.conf.clip.sources[0].type !== "application/x-mpegurl") {
                    this.startCalled = true;
                    //console.log("start() called from 'progress' event");
                    this.amSDKInstance.start();
                    this.onFPEvent({
                        type: "resume",
                        api: api,
                        data: data
                    });
                    //this.fPAPI.trigger("resume");
                }
                if (this.adInProgress === true) {
                    this.fPAPI.pause();
                }
                if (this.firstPlay) {
                    this.firstPlay = false;
                    //console.log("updateVideoState(PLAY) called from 'progress' event");

                    this.amSDKInstance.updateVideoState(ArtiVideoState.PLAY);
                    return;
                }
                if (data > this.latestPlayhead) {
                    this.latestPlayhead = data;
                }
                this.amSDKInstance.updateVideoTime(data);
                break;
            case "finish":
                this.amSDKInstance.updateVideoState(ArtiVideoState.STOP);
                break;
            case "pause":
                this.amSDKInstance.updateVideoState(ArtiVideoState.PAUSE);
                break;
            }
        }
    },
    
    FPReady: function (fPAPI) {
        "use strict";
        var that = this;
        
        $(this.vidContainer).css("pointer-events", "none");

        this.fPAPI = fPAPI;


        this.fPAPI.on("pause resume progress finish volume", function (event, api, data) {
            that.onFPEvent.call(that, event, api, data);
        });


        //this.fPAPI.conf.splash = false;

        this.copyControls();
    }

};

artimediaFP6 = new ArtiMediaFP6();